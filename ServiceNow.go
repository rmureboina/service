package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

func CreateChangeRequest() {

}

func GetChangeRequest() {
	client := &http.Client{}

	req, _ := http.NewRequest("GET", "https://honeyteksystemsdemo2.service-now.com//api/sn_chg_rest/v1/change/standard/1766f1de47410200e90d87e8dee490f6", nil)
	req.Header.Add("Accept", "application/json")
	req.SetBasicAuth("rmurebXXX", "A!xxxx")

	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("Error ")
		return
	}

	defer resp.Body.Close()
	resp_body, _ := ioutil.ReadAll(resp.Body)

	var c = string(resp_body)

	fmt.Println(c)

	if err != nil {
		log.Fatal(err)
	}

}

func UpdateChangeRequest() {

	client := &http.Client{}

	req, _ := http.NewRequest("PATCH", "https://honeyteksystemsdemo2.service-now.com/api/sn_chg_rest/v1/change/standard/2ede1b95db2b4410345c9db2ca961934?assignment_group=netowk", nil)
	req.Header.Add("Accept", "application/json")
	req.Header.Add("assignment_group", "netowk")
	req.SetBasicAuth("rmureboina", "A!xxx")

	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("Error")
		return
	}

	defer resp.Body.Close()
	resp_body, _ := ioutil.ReadAll(resp.Body)

	var c = string(resp_body)
	fmt.Println(c)

}

func DeleteChangeRequest() {

	client := &http.Client{}

	req, _ := http.NewRequest("DELETE", "https://honeyteksystemsdemo2.service-now.com/api/sn_chg_rest/v1/change/standard/2ede1b95db2b4410345c9db2ca961934", nil)
	req.Header.Add("Accept", "application/json")
	req.SetBasicAuth("rmureboina", "A!xxxx")

	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("Error")
		return
	}

	defer resp.Body.Close()
	resp_body, _ := ioutil.ReadAll(resp.Body)
	var c = string(resp_body)
	fmt.Println(c)

}

func main() {

	GetChangeRequest()
	//UpdateChangeRequest()
	//DeleteChangeRequest()

	//fmt.Println(x) //json

}
